require 'gitlab'

module Standup
  class ReportGenerator
    def initialize(settings)
      @settings = settings

      configure_gitlab_client
    end

    def gitlab_report!
      done      = []
      reviewed  = []
      config    = @settings.config
      to_date   = @settings.to_date
      from_date = @settings.from_date

      projects_query = {
        per_page: 10,
        scope: :owned,
        search: config['gitlab_project_search_pattern'],
        order_by: :last_activity_at,
        sort: :desc
      }

      mrs_query = {
        per_page: 100,
        state: :merged,
        order_by: :updated_at,
        sort: :desc
      }

      projects = Gitlab.projects(projects_query)

      projects.each do |project|
        begin
          merged_mrs = Gitlab.merge_requests(project.id, mrs_query)
        rescue Gitlab::Error::Forbidden
          # Don't seem to have acces to some MRs
          merged_mrs = []
        end

        merged_mrs.each do |mr|
          updated_at = Time.parse(mr.updated_at)
          author     = mr.author.username
          assignee   = mr.assignee && mr.assignee.username

          if updated_at < to_date &&
             updated_at > from_date

            if author == config[:username]
              done << "#{mr.title} (#{project.name})"
            elsif assignee == config[:username]
              reviewed << "#{mr.title} (#{project.name})"
            end
          end
        end
      end

      puts '----- DONE -----'
      puts done
      puts "----------------\n\n"

      puts '--- REVIEWED ---'
      puts reviewed
      puts "----------------\n\n"
    end

    private

    def configure_gitlab_client
      api_endpoint = @settings.config['gitlab_endpoint'] || 'https://gitlab.com/api/v3'.freeze

      Gitlab.configure do |c|
        c.endpoint = api_endpoint
        c.private_token = @settings.config['gitlab_access_token']
      end
    end
  end
end
