# Standup

This gem is a simple binary to help you generate reports of your work.

# Installation

- Install with `gem install stand`
- Generate a new token on https://gitlab.example.net/profile/personal_access_tokens with the "api" scope
- Create the file `$HOME/.standup/standup.yml` with the following content:

~~~ruby
gitlab_access_token: 'AbCdEfGhIjKlMnOp'
gitlab_endpoint: 'https://gitlab.example.net/api/v3'
~~~


# Usage

And you are good to go via

    > standup --username <gitlab_username>
    ----- DONE -----
    A Merge Request title 1 (project_name1)
    A Merge request title 2 (project_name2)
    ----------------

    --- REVIEWED ---
    A reviewed merge request title 1 (project_name2)
    ----------------

Forgot a standup two days ago? Just

    standup --from 2017-02-01 --to 2017-02-02 --username <gitlab_username>


# Integrations

For now, only Gitlab's API.

# License

MIT Licensed © Capitaine Train SAS
